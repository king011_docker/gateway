#!/usr/bin/env bash
Tags=(
    18.04
    20.04
)


function help(){
    echo "nginx docker bash"
    echo
    echo "Usage:"
    echo "  $0 [flags]"
    echo "  $0 [command]"
    echo
    echo "Available Commands:"
    echo "  help              help for $0"
    echo "  tags              display tag list"
    echo "  apply             apply template to Dockerfile"
    echo "  build             build Dockerfile"
    echo "  push              push to docker hub"
    echo
    echo "Flags:"
    echo "  -h, --help          help for $0"
}
function tags(){
    for tag in ${Tags[@]}
    do
        echo $tag
    done
}
function buildTag(){
    local tag="$1"
    local dir="$2"
    echo build $tag
    cd "$dir/docker/$tag" 
    sudo docker build -t "king011/gateway:$tag" .
}
function pushTag(){
    local tag="$1"
    echo push $tag
    sudo docker push "king011/gateway:$tag"
}
function apply(){
    local dir=$(cd $(dirname $BASH_SOURCE) && pwd)
    set -eu;
    for tag in ${Tags[@]}
    do
        echo apply $tag

        mkdir "$dir/docker/$tag" -p

        echo "FROM ubuntu:$tag" > "$dir/docker/$tag/Dockerfile"
        cat "$dir/Dockerfile" >> "$dir/docker/$tag/Dockerfile"

        cp "$dir/list.json" "$dir/docker/$tag/list.json"
    done
}
function build(){
    local dir=$(cd $(dirname $BASH_SOURCE) && pwd)
    set -eu;
    for tag in ${Tags[@]}
    do
        buildTag "$tag" "$dir"
    done
}
function push(){
    set -eu;
    for tag in ${Tags[@]}
    do
        pushTag "$tag"
    done
}
case "$1" in
    help|-h|--help)
        help
    ;;
    tags)
        tags
    ;;
    apply)
        apply
    ;;
    build)
        build
    ;;
    push)
        push
    ;;
    *)
        help
        exit 1
    ;;
esac