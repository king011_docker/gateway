RUN set -eux; \
    apt-get update; \
    apt-get install -y --no-install-recommends sudo iptables net-tools iputils-ping dnsutils ca-certificates openssh-server nginx; \
    mkdir /run/sshd; \
    useradd -m -U -s /bin/bash dev; \
    echo "dev:123" | chpasswd; \
    echo "dev ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers; \
    apt-get install -y --no-install-recommends bash-completion curl wget;\
    rm -rf /var/lib/apt/lists/*;

RUN set -eux; \
    mkdir /opt/watch; \
    curl -#Lo /a.tar.gz https://gitlab.com/king011/fuckccp/uploads/d9ee11b0b28f4eb09d75cdd0f7ca86e2/watch.tar.gz; \
    tar -zxvf /a.tar.gz -C /opt/watch; \
    rm /a.tar.gz;

COPY list.json /opt/watch/list.json
CMD ["/opt/watch/watch","-conf","/opt/watch/list.json"]